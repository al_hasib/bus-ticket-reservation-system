/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mNewProject;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 *
 * @author ANNONYMOUS
 */
@Entity
@Table(name = "bus_details", catalog = "newproject", schema = "")
@NamedQueries({
    @NamedQuery(name = "BusDetails.findAll", query = "SELECT b FROM BusDetails b")
    , @NamedQuery(name = "BusDetails.findByBusId", query = "SELECT b FROM BusDetails b WHERE b.busId = :busId")
    , @NamedQuery(name = "BusDetails.findByBusNo", query = "SELECT b FROM BusDetails b WHERE b.busNo = :busNo")
    , @NamedQuery(name = "BusDetails.findByBusSource", query = "SELECT b FROM BusDetails b WHERE b.busSource = :busSource")
    , @NamedQuery(name = "BusDetails.findByBusDest", query = "SELECT b FROM BusDetails b WHERE b.busDest = :busDest")
    , @NamedQuery(name = "BusDetails.findByBusDate", query = "SELECT b FROM BusDetails b WHERE b.busDate = :busDate")
    , @NamedQuery(name = "BusDetails.findByBusTime", query = "SELECT b FROM BusDetails b WHERE b.busTime = :busTime")
    , @NamedQuery(name = "BusDetails.findByBMovement", query = "SELECT b FROM BusDetails b WHERE b.bMovement = :bMovement")
    , @NamedQuery(name = "BusDetails.findByBSeat", query = "SELECT b FROM BusDetails b WHERE b.bSeat = :bSeat")
    , @NamedQuery(name = "BusDetails.findByBPrice", query = "SELECT b FROM BusDetails b WHERE b.bPrice = :bPrice")})
public class BusDetails implements Serializable {

    @Transient
    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);

    private static final long serialVersionUID = 1L;
    @Column(name = "bus_id")
    private Integer busId;
    @Id
    @Basic(optional = false)
    @Column(name = "bus_no")
    private String busNo;
    @Column(name = "bus_source")
    private String busSource;
    @Column(name = "bus_dest")
    private String busDest;
    @Column(name = "bus_date")
    @Temporal(TemporalType.DATE)
    private Date busDate;
    @Column(name = "bus_time")
    private String busTime;
    @Column(name = "b_movement")
    private String bMovement;
    @Column(name = "b_seat")
    private String bSeat;
    @Column(name = "b_price")
    private Integer bPrice;

    public BusDetails() {
    }

    public BusDetails(String busNo) {
        this.busNo = busNo;
    }

    public Integer getBusId() {
        return busId;
    }

    public void setBusId(Integer busId) {
        Integer oldBusId = this.busId;
        this.busId = busId;
        changeSupport.firePropertyChange("busId", oldBusId, busId);
    }

    public String getBusNo() {
        return busNo;
    }

    public void setBusNo(String busNo) {
        String oldBusNo = this.busNo;
        this.busNo = busNo;
        changeSupport.firePropertyChange("busNo", oldBusNo, busNo);
    }

    public String getBusSource() {
        return busSource;
    }

    public void setBusSource(String busSource) {
        String oldBusSource = this.busSource;
        this.busSource = busSource;
        changeSupport.firePropertyChange("busSource", oldBusSource, busSource);
    }

    public String getBusDest() {
        return busDest;
    }

    public void setBusDest(String busDest) {
        String oldBusDest = this.busDest;
        this.busDest = busDest;
        changeSupport.firePropertyChange("busDest", oldBusDest, busDest);
    }

    public Date getBusDate() {
        return busDate;
    }

    public void setBusDate(Date busDate) {
        Date oldBusDate = this.busDate;
        this.busDate = busDate;
        changeSupport.firePropertyChange("busDate", oldBusDate, busDate);
    }

    public String getBusTime() {
        return busTime;
    }

    public void setBusTime(String busTime) {
        String oldBusTime = this.busTime;
        this.busTime = busTime;
        changeSupport.firePropertyChange("busTime", oldBusTime, busTime);
    }

    public String getBMovement() {
        return bMovement;
    }

    public void setBMovement(String bMovement) {
        String oldBMovement = this.bMovement;
        this.bMovement = bMovement;
        changeSupport.firePropertyChange("BMovement", oldBMovement, bMovement);
    }

    public String getBSeat() {
        return bSeat;
    }

    public void setBSeat(String bSeat) {
        String oldBSeat = this.bSeat;
        this.bSeat = bSeat;
        changeSupport.firePropertyChange("BSeat", oldBSeat, bSeat);
    }

    public Integer getBPrice() {
        return bPrice;
    }

    public void setBPrice(Integer bPrice) {
        Integer oldBPrice = this.bPrice;
        this.bPrice = bPrice;
        changeSupport.firePropertyChange("BPrice", oldBPrice, bPrice);
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (busNo != null ? busNo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BusDetails)) {
            return false;
        }
        BusDetails other = (BusDetails) object;
        if ((this.busNo == null && other.busNo != null) || (this.busNo != null && !this.busNo.equals(other.busNo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mNewProject.BusDetails[ busNo=" + busNo + " ]";
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }
    
}
