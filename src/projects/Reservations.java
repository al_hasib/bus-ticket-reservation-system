package mNewProject;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class Reservations extends javax.swing.JFrame {

    Statement stm;
    ResultSet rs;
    PreparedStatement ps;
    String sql = "";
    String sql1 = "";
    DefaultTableModel dtm;

    public Reservations() {
        initComponents();
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    }

    public void getValue() {
        try {
            busName.setText(rs.getString("bus_no"));
            searchDate.setText(rs.getString("bus_date"));
            tPrice.setText(rs.getString("b_price"));
            bId.setText(rs.getString("bus_id"));
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }

    public void insert() {
        AdminLogin login = new AdminLogin();
        login.MyDriver();
        sql = "insert into seat_details (name, dest, dets_to, date, time, t_seat, total, seat_no, bus_id) values('"
                + busName.getText() + "','" + des.getSelectedItem() + "','" + from.getSelectedItem() + "','"
                + searchDate.getText() + "','" + time.getSelectedItem() + "'," + Integer.parseInt(totalSeat.getText()) + ","
                + Integer.parseInt(total.getText()) + ",'" + seatNo.getText() + "'," + Integer.parseInt(bId.getText()) + ")";
        try {
            stm = login.con.createStatement();
            stm.executeUpdate(sql);
            System.out.println(sql);
        } catch (SQLException sqle) {
            JOptionPane.showMessageDialog(null, sqle);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        entityManager = java.beans.Beans.isDesignTime() ? null : javax.persistence.Persistence.createEntityManagerFactory("newproject?zeroDateTimeBehavior=convertToNullPU").createEntityManager();
        busDetailsQuery = java.beans.Beans.isDesignTime() ? null : entityManager.createQuery("SELECT b FROM BusDetails b");
        busDetailsList = java.beans.Beans.isDesignTime() ? java.util.Collections.emptyList() : busDetailsQuery.getResultList();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        buy = new javax.swing.JButton();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jButton2 = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        tPrice = new javax.swing.JTextField();
        total = new javax.swing.JTextField();
        bId = new javax.swing.JTextField();
        jButton3 = new javax.swing.JButton();
        from = new javax.swing.JComboBox<>();
        des = new javax.swing.JComboBox<>();
        time = new javax.swing.JComboBox<>();
        busName = new javax.swing.JTextField();
        searchDate = new javax.swing.JTextField();
        l = new javax.swing.JLabel();
        seatNo = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        totalSeat = new javax.swing.JTextField();
        jPanel3 = new javax.swing.JPanel();
        a1 = new javax.swing.JCheckBox();
        a4 = new javax.swing.JCheckBox();
        a2 = new javax.swing.JCheckBox();
        a3 = new javax.swing.JCheckBox();
        b4 = new javax.swing.JCheckBox();
        b2 = new javax.swing.JCheckBox();
        b1 = new javax.swing.JCheckBox();
        b3 = new javax.swing.JCheckBox();
        c4 = new javax.swing.JCheckBox();
        c2 = new javax.swing.JCheckBox();
        c1 = new javax.swing.JCheckBox();
        c3 = new javax.swing.JCheckBox();
        d4 = new javax.swing.JCheckBox();
        d2 = new javax.swing.JCheckBox();
        d1 = new javax.swing.JCheckBox();
        d3 = new javax.swing.JCheckBox();
        e4 = new javax.swing.JCheckBox();
        e2 = new javax.swing.JCheckBox();
        e1 = new javax.swing.JCheckBox();
        e3 = new javax.swing.JCheckBox();
        jLabel3 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setFont(new java.awt.Font("Times New Roman", 3, 24)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("RESERVATION");
        jLabel1.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));
        jPanel1.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 760, 45));

        jPanel2.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));

        jLabel2.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel2.setText("Bus Name :");

        jLabel4.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel4.setText("Date :");

        buy.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        buy.setText("BUY");
        buy.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        buy.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buyActionPerformed(evt);
            }
        });

        jLabel7.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel7.setText("From :");

        jLabel8.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel8.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel8.setText("Destination:");

        jButton2.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jButton2.setText("CANCEL");
        jButton2.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));

        jLabel5.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel5.setText("Time :");

        jLabel9.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel9.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel9.setText(" Ticket Price:");

        jLabel10.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel10.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel10.setText("Total Seat :");

        jLabel11.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel11.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel11.setText("Total :");

        bId.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                bIdKeyReleased(evt);
            }
        });

        jButton3.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jButton3.setText("RESET");
        jButton3.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));

        from.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Dhaka", "Chittagong", "Sherpur", "Shylet", "Rajshahi" }));

        des.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Dhaka", "Chittagong", "Sherpur", "Shylet", "Rajshahi" }));

        time.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "-----------", "05.00 AM", "06.00 AM", "07.00 AM", "08.00 AM", "09.00 AM", "10.00 AM", "11.00 AM", "12.00 AM", "01.00 PM", "02.00 PM", "03.00 PM", "04.00 PM", "05.00 PM", "06.00 PM", "07.00 PM", "08.00 PM" }));
        time.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                timeActionPerformed(evt);
            }
        });

        l.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        l.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        l.setText("Seat No");

        jLabel12.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel12.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel12.setText("Bus ID:");

        totalSeat.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                totalSeatKeyReleased(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(42, 42, 42)
                .addComponent(buy, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(38, 38, 38)
                .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(44, 44, 44)
                .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(l, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(seatNo, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 21, Short.MAX_VALUE)
                        .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 80, Short.MAX_VALUE)
                            .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 80, Short.MAX_VALUE)
                            .addComponent(jLabel8, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 80, Short.MAX_VALUE)
                            .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, 80, Short.MAX_VALUE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(des, 0, 100, Short.MAX_VALUE)
                            .addComponent(time, 0, 100, Short.MAX_VALUE)
                            .addComponent(busName, javax.swing.GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE)
                            .addComponent(searchDate, javax.swing.GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jLabel9, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 95, Short.MAX_VALUE)
                            .addComponent(jLabel12, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel7, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 54, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(tPrice, javax.swing.GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE)
                    .addComponent(total, javax.swing.GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE)
                    .addComponent(bId, javax.swing.GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE)
                    .addComponent(from, 0, 100, Short.MAX_VALUE)
                    .addComponent(totalSeat, javax.swing.GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE))
                .addGap(21, 21, 21))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel8)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(from, javax.swing.GroupLayout.DEFAULT_SIZE, 26, Short.MAX_VALUE)
                        .addComponent(des, javax.swing.GroupLayout.DEFAULT_SIZE, 26, Short.MAX_VALUE)))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(searchDate, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel12, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(bId, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(busName, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(22, 22, 22)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(tPrice, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(time, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(totalSeat, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(l, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(seatNo, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(total, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel11))
                .addGap(49, 49, 49)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(buy)
                    .addComponent(jButton3)
                    .addComponent(jButton2))
                .addGap(82, 82, 82))
        );

        jPanel1.add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 50, 500, 350));

        jPanel3.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));

        a1.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        a1.setText("A1");
        a1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                a1MouseClicked(evt);
            }
        });

        a4.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        a4.setText("A4");
        a4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                a4MouseClicked(evt);
            }
        });

        a2.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        a2.setText("A2");
        a2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                a2MouseClicked(evt);
            }
        });

        a3.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        a3.setText("A3");
        a3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                a3MouseClicked(evt);
            }
        });

        b4.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        b4.setText("B4");
        b4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                b4MouseClicked(evt);
            }
        });

        b2.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        b2.setText("B2");
        b2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                b2MouseClicked(evt);
            }
        });

        b1.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        b1.setText("B1");
        b1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                b1MouseClicked(evt);
            }
        });
        b1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b1ActionPerformed(evt);
            }
        });

        b3.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        b3.setText("B3");
        b3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                b3MouseClicked(evt);
            }
        });

        c4.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        c4.setText("C4");
        c4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                c4MouseClicked(evt);
            }
        });

        c2.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        c2.setText("C2");
        c2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                c2MouseClicked(evt);
            }
        });

        c1.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        c1.setText("C1");
        c1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                c1MouseClicked(evt);
            }
        });

        c3.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        c3.setText("C3");
        c3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                c3MouseClicked(evt);
            }
        });

        d4.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        d4.setText("D4");
        d4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                d4MouseClicked(evt);
            }
        });

        d2.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        d2.setText("D2");
        d2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                d2MouseClicked(evt);
            }
        });

        d1.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        d1.setText("D1");
        d1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                d1MouseClicked(evt);
            }
        });

        d3.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        d3.setText("D3");
        d3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                d3MouseClicked(evt);
            }
        });

        e4.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        e4.setText("E4");
        e4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                e4MouseClicked(evt);
            }
        });

        e2.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        e2.setText("E2");
        e2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                e2MouseClicked(evt);
            }
        });

        e1.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        e1.setText("E1");
        e1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                e1MouseClicked(evt);
            }
        });

        e3.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        e3.setText("E3");
        e3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                e3MouseClicked(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Times New Roman", 1, 24)); // NOI18N
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel3.setText("Select Your Seat");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(d1, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(10, 10, 10)
                        .addComponent(d2, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(d4, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(16, 16, 16))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                        .addComponent(e1, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(10, 10, 10)
                        .addComponent(e2, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(e4, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(16, 16, 16))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel3Layout.createSequentialGroup()
                                .addComponent(a1, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(10, 10, 10)
                                .addComponent(a2, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 45, Short.MAX_VALUE)
                                .addComponent(a3, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(10, 10, 10)
                                .addComponent(a4, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel3Layout.createSequentialGroup()
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel3Layout.createSequentialGroup()
                                        .addComponent(b1, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(10, 10, 10)
                                        .addComponent(b2, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPanel3Layout.createSequentialGroup()
                                        .addComponent(c1, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(10, 10, 10)
                                        .addComponent(c2, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(c3, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(b3, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(d3, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(e3, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(10, 10, 10)
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(b4, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(c4, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGap(17, 17, 17))))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 34, Short.MAX_VALUE)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(a1)
                    .addComponent(a4)
                    .addComponent(a2)
                    .addComponent(a3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(b1)
                    .addComponent(b4)
                    .addComponent(b2)
                    .addComponent(b3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(c1)
                    .addComponent(c4)
                    .addComponent(c2)
                    .addComponent(c3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(d1)
                    .addComponent(d4)
                    .addComponent(d2)
                    .addComponent(d3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(e1)
                    .addComponent(e4)
                    .addComponent(e2)
                    .addComponent(e3))
                .addGap(142, 142, 142))
        );

        jPanel1.add(jPanel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 50, 260, -1));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void timeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_timeActionPerformed
        // TODO add your handling code here:
        AdminLogin login = new AdminLogin();
        login.MyDriver();
        sql = "select bus_no, bus_date, b_price , bus_id from bus_details where bus_source = ? and bus_dest = ? and bus_time = ?";
        try {
            ps = login.con.prepareStatement(sql);
            ps.setString(1, (String) from.getSelectedItem());
            ps.setString(2, (String) des.getSelectedItem());
            ps.setString(3, (String) time.getSelectedItem());
            rs = ps.executeQuery();
            if (rs.next()) {
                getValue();
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }//GEN-LAST:event_timeActionPerformed

    private void bIdKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_bIdKeyReleased
        // TODO add your handling code here:

    }//GEN-LAST:event_bIdKeyReleased

    private void buyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buyActionPerformed
        // TODO add your handling code here:
        AdminLogin login = new AdminLogin();
        login.MyDriver();
        String a;
        if (a1.isSelected() == true) {
            String add = "select a1 from booking where bus_id =  " + +Integer.parseInt(bId.getText());
            try {
                stm = login.con.createStatement();
                rs = stm.executeQuery(add);
                while (rs.next()) {
                    a = rs.getString("a1");
                    System.out.println(a);

                    if (a.equals("Not Booked")) {
                        insert();
                        sql1 = "update booking set a1 = 'Booked' where bus_id =" + Integer.parseInt(bId.getText());
                        stm = login.con.createStatement();
                        stm.executeUpdate(sql1);
                    } else {
                        JOptionPane.showMessageDialog(null, "A1 is Booked");
                    }
                }

            } catch (SQLException sqle) {
                JOptionPane.showMessageDialog(null, sqle);
            }
        }

        if (a2.isSelected() == true) {
            String add = "select a2 from booking where bus_id =  " + +Integer.parseInt(bId.getText());
            try {
                stm = login.con.createStatement();
                rs = stm.executeQuery(add);
                while (rs.next()) {
                    a = rs.getString("a2");
                    System.out.println(a);

                    if (a.equals("Not Booked")) {
                        insert();
                        sql1 = "update booking set a2 = 'Booked' where bus_id =" + Integer.parseInt(bId.getText());
                        stm = login.con.createStatement();
                        stm.executeUpdate(sql1);
                    } else {
                        JOptionPane.showMessageDialog(null, "A2 is Booked");
                    }
                }

            } catch (SQLException sqle) {
                JOptionPane.showMessageDialog(null, sqle);
            }
        }

        if (a3.isSelected() == true) {
            String add = "select a3 from booking where bus_id =  " + +Integer.parseInt(bId.getText());
            try {
                stm = login.con.createStatement();
                rs = stm.executeQuery(add);
                while (rs.next()) {
                    a = rs.getString("a3");
                    System.out.println(a);

                    if (a.equals("Not Booked")) {
                        insert();
                        sql1 = "update booking set a3 = 'Booked' where bus_id =" + Integer.parseInt(bId.getText());
                        stm = login.con.createStatement();
                        stm.executeUpdate(sql1);
                    } else {
                        JOptionPane.showMessageDialog(null, "A3 is Booked");
                    }
                }

            } catch (SQLException sqle) {
                JOptionPane.showMessageDialog(null, sqle);
            }
        }

        if (a4.isSelected() == true) {
            String add = "select a4 from booking where bus_id =  " + +Integer.parseInt(bId.getText());
            try {
                stm = login.con.createStatement();
                rs = stm.executeQuery(add);
                while (rs.next()) {
                    a = rs.getString("a4");
                    System.out.println(a);

                    if (a.equals("Not Booked")) {
                        insert();
                        sql1 = "update booking set a4 = 'Booked' where bus_id =" + Integer.parseInt(bId.getText());
                        stm = login.con.createStatement();
                        stm.executeUpdate(sql1);
                    } else {
                        JOptionPane.showMessageDialog(null, "A4 is Booked");
                    }
                }

            } catch (SQLException sqle) {
                JOptionPane.showMessageDialog(null, sqle);
            }
        }

        if (b1.isSelected() == true) {
            String add = "select b1 from booking where bus_id =  " + +Integer.parseInt(bId.getText());
            try {
                stm = login.con.createStatement();
                rs = stm.executeQuery(add);
                while (rs.next()) {
                    a = rs.getString("b1");
                    System.out.println(a);

                    if (a.equals("Not Booked")) {
                        insert();
                        sql1 = "update booking set b1 = 'Booked' where bus_id =" + Integer.parseInt(bId.getText());
                        stm = login.con.createStatement();
                        stm.executeUpdate(sql1);
                    } else {
                        JOptionPane.showMessageDialog(null, "B2 is Booked");
                    }
                }

            } catch (SQLException sqle) {
                JOptionPane.showMessageDialog(null, sqle);
            }
        }

        if (b2.isSelected() == true) {
            String add = "select b2 from booking where bus_id =  " + +Integer.parseInt(bId.getText());
            try {
                stm = login.con.createStatement();
                rs = stm.executeQuery(add);
                while (rs.next()) {
                    a = rs.getString("b2");
                    System.out.println(a);

                    if (a.equals("Not Booked")) {
                        insert();
                        sql1 = "update booking set b2 = 'Booked' where bus_id =" + Integer.parseInt(bId.getText());
                        stm = login.con.createStatement();
                        stm.executeUpdate(sql1);
                    } else {
                        JOptionPane.showMessageDialog(null, "B2 is Booked");
                    }
                }

            } catch (SQLException sqle) {
                JOptionPane.showMessageDialog(null, sqle);
            }
        }

        if (b3.isSelected() == true) {
            String add = "select b3 from booking where bus_id =  " + +Integer.parseInt(bId.getText());
            try {
                stm = login.con.createStatement();
                rs = stm.executeQuery(add);
                while (rs.next()) {
                    a = rs.getString("b3");
                    System.out.println(a);

                    if (a.equals("Not Booked")) {
                        insert();
                        sql1 = "update booking set b3 = 'Booked' where bus_id =" + Integer.parseInt(bId.getText());
                        stm = login.con.createStatement();
                        stm.executeUpdate(sql1);
                    } else {
                        JOptionPane.showMessageDialog(null, "B3 is Booked");
                    }
                }

            } catch (SQLException sqle) {
                JOptionPane.showMessageDialog(null, sqle);
            }
        }

        if (b4.isSelected() == true) {
            String add = "select b4 from booking where bus_id =  " + +Integer.parseInt(bId.getText());
            try {
                stm = login.con.createStatement();
                rs = stm.executeQuery(add);
                while (rs.next()) {
                    a = rs.getString("b4");
                    System.out.println(a);

                    if (a.equals("Not Booked")) {
                        insert();
                        sql1 = "update booking set b4 = 'Booked' where bus_id =" + Integer.parseInt(bId.getText());
                        stm = login.con.createStatement();
                        stm.executeUpdate(sql1);
                    } else {
                        JOptionPane.showMessageDialog(null, "B4 is Booked");
                    }
                }

            } catch (SQLException sqle) {
                JOptionPane.showMessageDialog(null, sqle);
            }
        }

        if (c1.isSelected() == true) {
            String add = "select c1 from booking where bus_id =  " + +Integer.parseInt(bId.getText());
            try {
                stm = login.con.createStatement();
                rs = stm.executeQuery(add);
                while (rs.next()) {
                    a = rs.getString("c1");
                    System.out.println(a);

                    if (a.equals("Not Booked")) {
                        insert();
                        sql1 = "update booking set c1 = 'Booked' where bus_id =" + Integer.parseInt(bId.getText());
                        stm = login.con.createStatement();
                        stm.executeUpdate(sql1);
                    } else {
                        JOptionPane.showMessageDialog(null, "C1 is Booked");
                    }
                }

            } catch (SQLException sqle) {
                JOptionPane.showMessageDialog(null, sqle);
            }
        }

        if (c2.isSelected() == true) {
            String add = "select c2 from booking where bus_id =  " + +Integer.parseInt(bId.getText());
            try {
                stm = login.con.createStatement();
                rs = stm.executeQuery(add);
                while (rs.next()) {
                    a = rs.getString("c2");
                    System.out.println(a);

                    if (a.equals("Not Booked")) {
                        insert();
                        sql1 = "update booking set c2 = 'Booked' where bus_id =" + Integer.parseInt(bId.getText());
                        stm = login.con.createStatement();
                        stm.executeUpdate(sql1);
                    } else {
                        JOptionPane.showMessageDialog(null, "C2 is Booked");
                    }
                }

            } catch (SQLException sqle) {
                JOptionPane.showMessageDialog(null, sqle);
            }
        }

        if (c3.isSelected() == true) {
            String add = "select c3 from booking where bus_id =  " + +Integer.parseInt(bId.getText());
            try {
                stm = login.con.createStatement();
                rs = stm.executeQuery(add);
                while (rs.next()) {
                    a = rs.getString("c3");
                    System.out.println(a);

                    if (a.equals("Not Booked")) {
                        insert();
                        sql1 = "update booking set c3 = 'Booked' where bus_id =" + Integer.parseInt(bId.getText());
                        stm = login.con.createStatement();
                        stm.executeUpdate(sql1);
                    } else {
                        JOptionPane.showMessageDialog(null, "C3 is Booked");
                    }
                }

            } catch (SQLException sqle) {
                JOptionPane.showMessageDialog(null, sqle);
            }
        }

        if (c4.isSelected() == true) {
            String add = "select c4 from booking where bus_id =  " + +Integer.parseInt(bId.getText());
            try {
                stm = login.con.createStatement();
                rs = stm.executeQuery(add);
                while (rs.next()) {
                    a = rs.getString("c4");
                    System.out.println(a);

                    if (a.equals("Not Booked")) {
                        insert();
                        sql1 = "update booking set c4 = 'Booked' where bus_id =" + Integer.parseInt(bId.getText());
                        stm = login.con.createStatement();
                        stm.executeUpdate(sql1);
                    } else {
                        JOptionPane.showMessageDialog(null, "C4 is Booked");
                    }
                }

            } catch (SQLException sqle) {
                JOptionPane.showMessageDialog(null, sqle);
            }
        }

        if (d1.isSelected() == true) {
            String add = "select d1 from booking where bus_id =  " + +Integer.parseInt(bId.getText());
            try {
                stm = login.con.createStatement();
                rs = stm.executeQuery(add);
                while (rs.next()) {
                    a = rs.getString("d1");
                    System.out.println(a);

                    if (a.equals("Not Booked")) {
                        insert();
                        sql1 = "update booking set d1 = 'Booked' where bus_id =" + Integer.parseInt(bId.getText());
                        stm = login.con.createStatement();
                        stm.executeUpdate(sql1);
                    } else {
                        JOptionPane.showMessageDialog(null, "D1 is Booked");
                    }
                }

            } catch (SQLException sqle) {
                JOptionPane.showMessageDialog(null, sqle);
            }
        }

        if (d2.isSelected() == true) {
            String add = "select d2 from booking where bus_id =  " + +Integer.parseInt(bId.getText());
            try {
                stm = login.con.createStatement();
                rs = stm.executeQuery(add);
                while (rs.next()) {
                    a = rs.getString("d2");
                    System.out.println(a);

                    if (a.equals("Not Booked")) {
                        insert();
                        sql1 = "update booking set d2 = 'Booked' where bus_id =" + Integer.parseInt(bId.getText());
                        stm = login.con.createStatement();
                        stm.executeUpdate(sql1);
                    } else {
                        JOptionPane.showMessageDialog(null, "D2 is Booked");
                    }
                }

            } catch (SQLException sqle) {
                JOptionPane.showMessageDialog(null, sqle);
            }
        }

        if (d3.isSelected() == true) {
            String add = "select d3 from booking where bus_id =  " + +Integer.parseInt(bId.getText());
            try {
                stm = login.con.createStatement();
                rs = stm.executeQuery(add);
                while (rs.next()) {
                    a = rs.getString("d3");
                    System.out.println(a);

                    if (a.equals("Not Booked")) {
                        insert();
                        sql1 = "update booking set d3 = 'Booked' where bus_id =" + Integer.parseInt(bId.getText());
                        stm = login.con.createStatement();
                        stm.executeUpdate(sql1);
                    } else {
                        JOptionPane.showMessageDialog(null, "D3 is Booked");
                    }
                }

            } catch (SQLException sqle) {
                JOptionPane.showMessageDialog(null, sqle);
            }
        }

        if (d4.isSelected() == true) {
            String add = "select d4 from booking where bus_id =  " + +Integer.parseInt(bId.getText());
            try {
                stm = login.con.createStatement();
                rs = stm.executeQuery(add);
                while (rs.next()) {
                    a = rs.getString("d4");
                    System.out.println(a);

                    if (a.equals("Not Booked")) {
                        insert();
                        sql1 = "update booking set d4 = 'Booked' where bus_id =" + Integer.parseInt(bId.getText());
                        stm = login.con.createStatement();
                        stm.executeUpdate(sql1);
                    } else {
                        JOptionPane.showMessageDialog(null, "D4 is Booked");
                    }
                }

            } catch (SQLException sqle) {
                JOptionPane.showMessageDialog(null, sqle);
            }
        }
        
        if (e1.isSelected() == true) {
            String add = "select e1 from booking where bus_id =  " + +Integer.parseInt(bId.getText());
            try {
                stm = login.con.createStatement();
                rs = stm.executeQuery(add);
                while (rs.next()) {
                    a = rs.getString("e1");
                    System.out.println(a);

                    if (a.equals("Not Booked")) {
                        insert();
                        sql1 = "update booking set e1 = 'Booked' where bus_id =" + Integer.parseInt(bId.getText());
                        stm = login.con.createStatement();
                        stm.executeUpdate(sql1);
                    } else {
                        JOptionPane.showMessageDialog(null, "E1 is Booked");
                    }
                }

            } catch (SQLException sqle) {
                JOptionPane.showMessageDialog(null, sqle);
            }
        }

        if (e2.isSelected() == true) {
            String add = "select e2 from booking where bus_id =  " + +Integer.parseInt(bId.getText());
            try {
                stm = login.con.createStatement();
                rs = stm.executeQuery(add);
                while (rs.next()) {
                    a = rs.getString("e2");
                    System.out.println(a);

                    if (a.equals("Not Booked")) {
                        insert();
                        sql1 = "update booking set e2 = 'Booked' where bus_id =" + Integer.parseInt(bId.getText());
                        stm = login.con.createStatement();
                        stm.executeUpdate(sql1);
                    } else {
                        JOptionPane.showMessageDialog(null, "E2 is Booked");
                    }
                }

            } catch (SQLException sqle) {
                JOptionPane.showMessageDialog(null, sqle);
            }
        }

        if (e3.isSelected() == true) {
            String add = "select e3 from booking where bus_id =  " + +Integer.parseInt(bId.getText());
            try {
                stm = login.con.createStatement();
                rs = stm.executeQuery(add);
                while (rs.next()) {
                    a = rs.getString("e3");
                    System.out.println(a);

                    if (a.equals("Not Booked")) {
                        insert();
                        sql1 = "update booking set e3 = 'Booked' where bus_id =" + Integer.parseInt(bId.getText());
                        stm = login.con.createStatement();
                        stm.executeUpdate(sql1);
                    } else {
                        JOptionPane.showMessageDialog(null, "E3 is Booked");
                    }
                }

            } catch (SQLException sqle) {
                JOptionPane.showMessageDialog(null, sqle);
            }
        }

        if (e4.isSelected() == true) {
            String add = "select e4 from booking where bus_id =  " + +Integer.parseInt(bId.getText());
            try {
                stm = login.con.createStatement();
                rs = stm.executeQuery(add);
                while (rs.next()) {
                    a = rs.getString("e4");
                    System.out.println(a);

                    if (a.equals("Not Booked")) {
                        insert();
                        sql1 = "update booking set e4 = 'Booked' where bus_id =" + Integer.parseInt(bId.getText());
                        stm = login.con.createStatement();
                        stm.executeUpdate(sql1);
                    } else {
                        JOptionPane.showMessageDialog(null, "E4 is Booked");
                    }
                }

            } catch (SQLException sqle) {
                JOptionPane.showMessageDialog(null, sqle);
            }
        }

    }//GEN-LAST:event_buyActionPerformed

    private void a1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_a1MouseClicked
        // TODO add your handling code here:
        String a = seatNo.getText() + " " + a1.getText();
        seatNo.setText(a);
    }//GEN-LAST:event_a1MouseClicked

    private void a2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_a2MouseClicked
        // TODO add your handling code here:
        String a = seatNo.getText() + " " + a2.getText();
        seatNo.setText(a);
    }//GEN-LAST:event_a2MouseClicked

    private void a3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_a3MouseClicked
        // TODO add your handling code here:
        String a = seatNo.getText() + " " + a3.getText();
        seatNo.setText(a);
    }//GEN-LAST:event_a3MouseClicked

    private void a4MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_a4MouseClicked
        // TODO add your handling code here:
        String a = seatNo.getText() + " " + a4.getText();
        seatNo.setText(a);
    }//GEN-LAST:event_a4MouseClicked

    private void b1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_b1MouseClicked
        // TODO add your handling code here:
        String a = seatNo.getText() + " " + b1.getText();
        seatNo.setText(a);
    }//GEN-LAST:event_b1MouseClicked

    private void b2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_b2MouseClicked
        // TODO add your handling code here:
        String a = seatNo.getText() + " " + b2.getText();
        seatNo.setText(a);
    }//GEN-LAST:event_b2MouseClicked

    private void b3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_b3MouseClicked
        // TODO add your handling code here:
        String a = seatNo.getText() + " " + b3.getText();
        seatNo.setText(a);
    }//GEN-LAST:event_b3MouseClicked

    private void b4MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_b4MouseClicked
        // TODO add your handling code here:
        String a = seatNo.getText() + " " + b4.getText();
        seatNo.setText(a);
    }//GEN-LAST:event_b4MouseClicked

    private void c1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_c1MouseClicked
        // TODO add your handling code here:
        String a = seatNo.getText() + " " + c1.getText();
        seatNo.setText(a);
    }//GEN-LAST:event_c1MouseClicked

    private void c2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_c2MouseClicked
        // TODO add your handling code here:
        String a = seatNo.getText() + " " + c2.getText();
        seatNo.setText(a);
    }//GEN-LAST:event_c2MouseClicked

    private void c3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_c3MouseClicked
        // TODO add your handling code here:
        String a = seatNo.getText() + " " + c3.getText();
        seatNo.setText(a);
    }//GEN-LAST:event_c3MouseClicked

    private void c4MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_c4MouseClicked
        // TODO add your handling code here:
        String a = seatNo.getText() + " " + c4.getText();
        seatNo.setText(a);
    }//GEN-LAST:event_c4MouseClicked

    private void d1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_d1MouseClicked
        // TODO add your handling code here:
        String a = seatNo.getText() + " " + d1.getText();
        seatNo.setText(a);
    }//GEN-LAST:event_d1MouseClicked

    private void d2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_d2MouseClicked
        // TODO add your handling code here:
        String a = seatNo.getText() + " " + d2.getText();
        seatNo.setText(a);
    }//GEN-LAST:event_d2MouseClicked

    private void d3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_d3MouseClicked
        // TODO add your handling code here:
        String a = seatNo.getText() + " " + d3.getText();
        seatNo.setText(a);
    }//GEN-LAST:event_d3MouseClicked

    private void d4MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_d4MouseClicked
        // TODO add your handling code here:
        String a = seatNo.getText() + " " + d4.getText();
        seatNo.setText(a);
    }//GEN-LAST:event_d4MouseClicked

    private void totalSeatKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_totalSeatKeyReleased
        // TODO add your handling code here:
        int a = Integer.parseInt(tPrice.getText());
        int b = Integer.parseInt(totalSeat.getText());
        int c = a * b;
        total.setText(String.valueOf(c));

    }//GEN-LAST:event_totalSeatKeyReleased

    private void b1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_b1ActionPerformed

    private void e1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_e1MouseClicked
        // TODO add your handling code here:
        String a = seatNo.getText() + " " + e1.getText();
        seatNo.setText(a);
    }//GEN-LAST:event_e1MouseClicked

    private void e2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_e2MouseClicked
        // TODO add your handling code here:
        String a = seatNo.getText() + " " + e2.getText();
        seatNo.setText(a);
    }//GEN-LAST:event_e2MouseClicked

    private void e3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_e3MouseClicked
        // TODO add your handling code here:
        String a = seatNo.getText() + " " + e3.getText();
        seatNo.setText(a);
    }//GEN-LAST:event_e3MouseClicked

    private void e4MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_e4MouseClicked
        // TODO add your handling code here:
        String a = seatNo.getText() + " " + e4.getText();
        seatNo.setText(a);
    }//GEN-LAST:event_e4MouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Reservations.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Reservations.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Reservations.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Reservations.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Reservations().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JCheckBox a1;
    private javax.swing.JCheckBox a2;
    private javax.swing.JCheckBox a3;
    private javax.swing.JCheckBox a4;
    private javax.swing.JCheckBox b1;
    private javax.swing.JCheckBox b2;
    private javax.swing.JCheckBox b3;
    private javax.swing.JCheckBox b4;
    private javax.swing.JTextField bId;
    private java.util.List<mNewProject.BusDetails> busDetailsList;
    private javax.persistence.Query busDetailsQuery;
    private javax.swing.JTextField busName;
    private javax.swing.JButton buy;
    private javax.swing.JCheckBox c1;
    private javax.swing.JCheckBox c2;
    private javax.swing.JCheckBox c3;
    private javax.swing.JCheckBox c4;
    private javax.swing.JCheckBox d1;
    private javax.swing.JCheckBox d2;
    private javax.swing.JCheckBox d3;
    private javax.swing.JCheckBox d4;
    private javax.swing.JComboBox<String> des;
    private javax.swing.JCheckBox e1;
    private javax.swing.JCheckBox e2;
    private javax.swing.JCheckBox e3;
    private javax.swing.JCheckBox e4;
    private javax.persistence.EntityManager entityManager;
    private javax.swing.JComboBox<String> from;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JLabel l;
    private javax.swing.JTextField searchDate;
    private javax.swing.JTextField seatNo;
    private javax.swing.JTextField tPrice;
    private javax.swing.JComboBox<String> time;
    private javax.swing.JTextField total;
    private javax.swing.JTextField totalSeat;
    // End of variables declaration//GEN-END:variables
}
