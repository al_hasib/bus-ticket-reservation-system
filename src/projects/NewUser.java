/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mNewProject;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.regex.Pattern;
import javax.swing.JOptionPane;

/**
 *
 * @author ANNONYMOUS
 */
public class NewUser extends javax.swing.JFrame {

    AdminLogin adm = new AdminLogin();
    Statement stm;
    ResultSet rs;
    PreparedStatement ps;
    String sql = "";
    String fName;
    String lName;
    String upperCaseChars = "(.*[A-Z].*)";
    String lowerCaseChars = "(.*[a-z].*)";
    String numbers = "(.*[0-9].*)";

    public NewUser() {
        initComponents();
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    }

    private void insert() throws SQLException {
        adm.MyDriver();
        fName = f_Name.getText();
        lName = l_Name.getText();
        Date d = dob.getDate();
        java.text.SimpleDateFormat smd = new java.text.SimpleDateFormat("YYYY-MM-dd");
        String dobs = smd.format(d);

        try {

            sql = "insert into user_info values('" + f_Name.getText() + "','"
                    + l_Name.getText() + "','" + dobs
                    + "','" + email.getText() + "','" + password.getText() + "')";

            if (f_Name.getText().isEmpty()) {
                JOptionPane.showMessageDialog(null, "First name can't be empty", "Worning..!!!", JOptionPane.ERROR_MESSAGE);
                f_Name.requestFocus();

                if (l_Name.getText().isEmpty()) {
                    JOptionPane.showMessageDialog(null, "Last name can't be empty", "Worning..!!!", JOptionPane.ERROR_MESSAGE);
                    l_Name.requestFocus();

                    if (dobs.isEmpty()) {
                        JOptionPane.showMessageDialog(null, "Please select your Date of Birth", "Worning..!!!", JOptionPane.ERROR_MESSAGE);
                        dob.requestFocus();

                        if (!(Pattern.matches("^[a-zA-Z0-9-_]+[@]+[a-z]+[.]+[com]+$", email.getText()))) {
                            JOptionPane.showMessageDialog(null, "Please enter a Valid email", "Worning..!!!", JOptionPane.ERROR_MESSAGE);
                            email.requestFocus();

                            if ((password.getText().length() > 15 | password.getText().length() < 7) && (!password.getText().matches(upperCaseChars)) && (!password.getText().matches(lowerCaseChars))) {
                                JOptionPane.showMessageDialog(null, "Please Enter A Valid Password", "Worning..!!!", JOptionPane.ERROR_MESSAGE);
                                password.requestFocus();

                                if (!password.equals(password1)) {
                                    JOptionPane.showMessageDialog(null, "Passwords dont match.", "Worning..!!!", JOptionPane.ERROR_MESSAGE);
                                    password1.requestFocus();
                                }
                            }
                        }
                    }
                }
            } else {
                stm = adm.con.createStatement();
                stm.executeUpdate(sql);
                JOptionPane.showMessageDialog(null, "Signup Successful");
            }
        } catch (SQLException sqle) {
            JOptionPane.showMessageDialog(null, sqle);
        }

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        uName = new javax.swing.JLabel();
        f_Name = new javax.swing.JTextField();
        uName1 = new javax.swing.JLabel();
        l_Name = new javax.swing.JTextField();
        uName2 = new javax.swing.JLabel();
        uName3 = new javax.swing.JLabel();
        password = new javax.swing.JPasswordField();
        password1 = new javax.swing.JPasswordField();
        uName4 = new javax.swing.JLabel();
        uName5 = new javax.swing.JLabel();
        email = new javax.swing.JTextField();
        login = new javax.swing.JButton();
        reset = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        dob = new com.toedter.calendar.JDateChooser();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setFont(new java.awt.Font("Times New Roman", 1, 36)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("New User Register");

        uName.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        uName.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        uName.setText("First Name :");

        uName1.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        uName1.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        uName1.setText("E-mail :");

        uName2.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        uName2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        uName2.setText("Last Name :");

        uName3.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        uName3.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        uName3.setText("Password :");

        uName4.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        uName4.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        uName4.setText("Re-Password :");

        uName5.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        uName5.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        uName5.setText("Date Of Birth :");

        login.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        login.setText("Signup");
        login.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                loginActionPerformed(evt);
            }
        });

        reset.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        reset.setText("Reset");

        jButton4.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jButton4.setText("Cancel");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(38, 38, 38)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(uName4, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 133, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(uName3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 133, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(uName5, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 133, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(uName1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(uName2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(uName, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(email)
                            .addComponent(l_Name)
                            .addComponent(f_Name)
                            .addComponent(password)
                            .addComponent(password1)
                            .addComponent(dob, javax.swing.GroupLayout.DEFAULT_SIZE, 144, Short.MAX_VALUE))
                        .addGap(58, 58, 58))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(28, 28, 28)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jButton4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(login, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(reset, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(78, 78, 78)))
                .addGap(37, 37, 37))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(27, 27, 27)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(uName)
                    .addComponent(f_Name, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(uName2)
                    .addComponent(l_Name, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(uName5)
                    .addComponent(dob, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(15, 15, 15)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(email, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(uName1))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(uName3)
                    .addComponent(password, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(uName4)
                    .addComponent(password1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(login)
                    .addComponent(reset))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton4, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(27, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void loginActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_loginActionPerformed
        try {
            insert();
        } catch (SQLException sqle) {
            JOptionPane.showMessageDialog(null, sqle);
        }
    }//GEN-LAST:event_loginActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        dispose();
        UserLogin ul = new UserLogin();
        ul.setVisible(true);
        ul.setLocationRelativeTo(null);
        
    }//GEN-LAST:event_jButton4ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(NewUser.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(NewUser.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(NewUser.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(NewUser.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new NewUser().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private com.toedter.calendar.JDateChooser dob;
    private javax.swing.JTextField email;
    private javax.swing.JTextField f_Name;
    private javax.swing.JButton jButton4;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JTextField l_Name;
    private javax.swing.JButton login;
    private javax.swing.JPasswordField password;
    private javax.swing.JPasswordField password1;
    private javax.swing.JButton reset;
    private javax.swing.JLabel uName;
    private javax.swing.JLabel uName1;
    private javax.swing.JLabel uName2;
    private javax.swing.JLabel uName3;
    private javax.swing.JLabel uName4;
    private javax.swing.JLabel uName5;
    // End of variables declaration//GEN-END:variables
}
